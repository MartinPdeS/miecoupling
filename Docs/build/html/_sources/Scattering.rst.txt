Classes
=================


Scatterering
------------
.. automodule:: PyMieCoupling.classes.Scattering
    :members:
    :undoc-members:
    :show-inheritance:  
  
Detector
-------- 
.. automodule:: PyMieCoupling.classes.Detector
    :members:
    :undoc-members:
    :show-inheritance:

Meshes
------
.. automodule:: PyMieCoupling.classes.Meshes
    :members:
    :undoc-members:
    :show-inheritance:
