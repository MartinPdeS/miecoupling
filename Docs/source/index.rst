.. PyMieSim documentation master file, created by
   sphinx-quickstart on Wed Feb  3 17:01:20 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyMieSim's documentation!
====================================

.. toctree::
   :maxdepth: 3
   :caption: Table of contents:

   Intro
   Theory
   Code
   Examples


To-Do List
----------

- Adding docstring
- Adding monotonic metric to optimizer class
- Comments on c++ codes



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`






Author Contact Information
--------------------------
PyMieSim was written by `Martin Poinsinet de Sivry-Houle <https://github.com/MartinPdS>`_.

Email: `martin.poinsinet-de-sivry@polymtl.ca <mailto:martin.poinsinet-de-sivry@polymtl.ca?subject=PyMieSim>`_
