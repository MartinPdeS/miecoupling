Classes
=================


Class: Scatterering
-------------------
.. automodule:: PyMieSim.Scatterer
    :members:
    :undoc-members:
    :inherited-members:

Class: Detector
---------------
.. automodule:: PyMieSim.Detector
    :members:
    :undoc-members:
    :inherited-members:

Class: Mesh
-----------
.. automodule:: PyMieSim.Mesh
    :members:
    :undoc-members:
    :inherited-members:

Class: Sets
-----------
.. automodule:: PyMieSim.Sets
    :members:
    :undoc-members:
    :show-inheritance:

Class: Representations
----------------------
.. automodule:: PyMieSim.Representations
    :members:
    :undoc-members:
    :show-inheritance:

Class: DataFrame
----------------
.. automodule:: PyMieSim.DataFrame
    :members:
    :undoc-members:
    :show-inheritance:


Function: Coupling
------------------

Function: CenteredCoupling
--------------------------

Function: MeanCoupling
----------------------

Function: Footprint
-------------------
